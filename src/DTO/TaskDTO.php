<?php

namespace App\DTO;


class TaskDTO
{

    const TASK_CHANGED_EVENT = "task.event.changed";
//id, userId, status(new, complete, cancel), title, description) and REST for IT
    private ?int $id;

    private ?int $userId;

    private ?string $status;

    public function __construct(int $id, int $userId, string $status)
    {
        $this->id = $id;
        $this->userId = $userId;
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @param int|null $userId
     */
    public function setUserId(?int $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */

    public function __toString(): string
    {
       return  "id = " . $this->getId() . ",user id = " . $this->userId . ', status = ' . $this->getStatus();
    }

}