<?php

namespace App\DTO;

class UserDTO
{
    //id, name, phone, completedTasks(int), active

    private ?int $id;
    private ?string $name;
    private ?string $phone;
    private ?int $completedTasks;
    private ?int $active;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     */
    public function setPhone(?string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return int|null
     */
    public function getCompletedTasks(): ?int
    {
        return $this->completedTasks;
    }

    /**
     * @param int|null $completedTasks
     */
    public function setCompletedTasks(?int $completedTasks): void
    {
        $this->completedTasks = $completedTasks;
    }

    /**
     * @return int|null
     */
    public function getActive(): ?int
    {
        return $this->active;
    }

    /**
     * @param int|null $active
     */
    public function setActive(?int $active): void
    {
        $this->active = $active;
    }


}