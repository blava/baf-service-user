<?php

namespace App\MessageHandler;

use App\DTO\TaskDTO;
use App\Message\TaskPayload;
use App\Repository\UserRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Envelope;

#[AsMessageHandler]
class TaskPayloadHandler
{
    private $repository;

    private LoggerInterface $logger;
    public function __construct(UserRepository $repository, LoggerInterface $logger)
    {
        $this->repository = $repository;
        $this->logger = $logger;
        $this->logger->debug("Constructor : " . __FILE__);
    }

    public function __invoke(TaskDTO $task)
    {
//        payload {"id": "taskuuid", "userId": "uuid", "status": "new_status"}
//        User Service listen `task.event.status-changed` topic and if `status === 'completed'` increments user.completedTasks field
        $id = $task->getUserId();
        $user = $this->repository->find($id);
        if ($task->getStatus() == "complete") {
            $user->setCompletedTasks($user->getCompletedTasks() + 1);
            $this->repository->save($user, true);
        }
    }

}