<?php

namespace App\Event;

use App\DTO\UserDTO;
use App\Entity\User;
use App\Message\UserPayload;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class UserCreatedEventSubscriber implements EventSubscriberInterface
{

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public static function getSubscribedEvents()
    {
        return [
            UserCreatedEvent::USER_CREATED_EVENT => 'onCreatePublished'
        ];
    }

    /**
     * @param UserCreatedEvent $createdEvent
     * @param MessageBusInterface $messageBus
     * @return void
     */
    public function onCreatePublished(UserCreatedEvent $event): void
    {
        if ($event->getUser()->getCompletedTasks() === 0)
            $this->messageBus->dispatch(new UserPayload($event->getuser()->getId(), $event->getUser()->getName()));
        else dump($event);
    }
}