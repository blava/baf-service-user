<?php

namespace App\Event;

use App\Mapper\MapperConverter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class UserEventSubscriber implements EventSubscriberInterface
{
    private MapperConverter $mapper;
    public function __construct (MapperConverter $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @inheritDoc
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onRequestEvent'
        ];
    }

    public function onRequestEvent(RequestEvent $event) : bool
    {
        $request = $event->getRequest();
        $user = $this->mapper->map($request);
        $request->attributes->set('user_dto', $user);
        return true;
    }
}