<?php

namespace App\Event;

use App\Entity\User;
use Symfony\Component\EventDispatcher\EventDispatcher;

class UserCreatedEvent extends EventDispatcher
{
    private User $user;
    const USER_CREATED_EVENT = 'some.internal.event';

    public function __construct(User $user)
    {
        parent::__construct();
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }

}