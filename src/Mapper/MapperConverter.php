<?php

namespace App\Mapper;

use App\DTO\UserDTO;
use App\Service\ValidationInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class MapperConverter
{
private SerializerInterface $serializer;
private ValidationInterface $validation;

public function __construct(SerializerInterface $serializer, ValidationInterface $validation)
{
    $this->serializer = $serializer;
    $this->validation = $validation;
}

    /**
     * @throws \Exception
     */
    public function map(Request $request)
    {
        $user = $this->serializer->deserialize($request->getContent(), UserDTO::class, 'json');
        $response = $this->validation->validate($user);
        if (!$response['success'])
            throw new \Exception("Invalid User " );
        return $user;
    }


}