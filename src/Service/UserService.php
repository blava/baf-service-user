<?php

namespace App\Service;

use App\DTO\UserDTO;
use App\Entity\User;
use App\Event\UserCreatedEvent;
use App\Message\UserPayload;
use App\Repository\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class UserService
{

       public function __construct(
           private UserRepository $repository,
           private EventDispatcherInterface $dispatcher)
       {
       }

    public function saveUserInDB(UserDTO $dto)
    {
        $user = new User();
        $user->setName($dto->getName());
        $user->setPhone($dto->getPhone());
        $user->setActive($dto->getActive());
        $user->setCompletedTasks(0);
        $this->repository->save($user,true);

        $newUserPayload = new UserCreatedEvent($user);
        $this->dispatcher->dispatch( $newUserPayload, UserCreatedEvent::USER_CREATED_EVENT);

   }
}