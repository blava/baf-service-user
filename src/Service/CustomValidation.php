<?php

namespace App\Service;

use App\DTO\GeneralUserInterface;
use App\DTO\UserDTO;
use Symfony\Component\HttpFoundation\Response;

class CustomValidation implements ValidationInterface
{
    public function validate(UserDTO $user) : array
    {

        $response = array('success' => false, 'message' => array());
        if (!$user->getName())
            $response['message'][] ='Username is empty';
        if (!$this->phoneValidation($user->getPhone()))
            $response['message'][] = 'Invalid Phone';
        if (!is_numeric($user->getCompletedTasks()))
            $response['message'][] = "Invalid completed task format";
        if (!$this->statusValidation($user->getActive()))
            $response['message'][] = "Invalid status value";
        $response['success'] = empty($response['message']);
        return $response;
    }

    public function phoneValidation(string $phone) : bool
    {
        return preg_match('/^[0-9]{9,14}\z/', $phone);
    }

    public function statusValidation(int $status)
    {
        return ($status == 0 || $status == 1);
    }
}