<?php

namespace App\Controller;

use App\DTO\UserDTO;
use App\Entity\User;
use App\Mapper\MapperConverter;
use App\Message\UserPayload;
use App\Repository\UserRepository;
use App\Service\UserService;
use App\Service\ValidationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class UserServiceController extends AbstractController
{
    private UserService $userService;
    private SerializerInterface $serializer;
    private ValidationInterface $validation;
    private MapperConverter $mapperConverter;
    private UserService $service;

    public function __construct(
        UserService         $userService,
        SerializerInterface $serializer,
        ValidationInterface $validation,
        MapperConverter     $mapperConverter,
        UserService         $service
    )
    {
        $this->userService = $userService;
        $this->serializer = $serializer;
        $this->validation = $validation;
        $this->mapperConverter = $mapperConverter;
        $this->service = $service;
    }

    #[Route('/user', name: 'create_user', methods: 'POST')]
    public function create(Request $request): Response
    {
        $dto = $this->mapperConverter->map($request);
        $this->service->saveUserInDB($dto);

        return new Response("User Created", 201);
    }

    #[Route('/user/{id}', name: 'show_user', methods: 'GET')]
    public function show(Request $request, int $id, UserRepository $repository, SerializerInterface $serializer): Response
    {
    }

    #[Route('/user/{id}', name: 'update_user', methods: 'PUT')]
    public function update(Request $request, EntityManagerInterface $entityManager, UserRepository $repository, int $id): Response
    {
        $user = $repository->find($id);
        $user->setName($request->get('name'));
        $user->setPhone($request->get('phone'));
        $entityManager->persist($user);
        $entityManager->flush();

        return new Response("user updated", 201);
    }


    #[Route('/user/{id}', name: 'delete_user', methods: 'DELETE')]
    public function delete(int $id, EntityManagerInterface $entityManager, UserRepository $repository): Response
    {
        $user = $repository->find($id);
        $entityManager->remove($user);
        $entityManager->flush();

        return new Response("User has been deleted", 200);
    }
}
