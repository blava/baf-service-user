# baf-service-user



## Installation instructions

- For proper functioning, needs database "bafood_user"
- Symfony server needs to be on port 8000
- URI for creating a user is http://localhost:8000/user with POST method and phone and name parameters
- Uses rabbit-mq for message exchange



## Work Details

After creating user, message is being transferred to the server http://localhost:9000 (Server B), where server receives message and creates the task with specified userId and sets status to new.
After updating the status , server http://localhost:9000 sends message to the server http://localhost:8000 (server A).

Server A checks user(if it's exist and active), after checks the status, if it's equals to 'complete' , increases completed_tasks count for specified user
